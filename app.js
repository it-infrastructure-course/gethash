var express = require('express');
const crypto = require('crypto');
const bodyParser = require('body-parser');
var qr = require('qr-image');
//var Redis = require("ioredis");

// uncomment for cluster use
// var cluster = new Redis.Cluster([
//   {
//     port: 6379,
//     host: "redis-cluster",
//     showFriendlyErrorStack: true,
//     scaleReads: "all"
//   }
// ]);

// comment for cluster use
//const cluster = new Redis(6379, "rl-redis.wi.fh-flensburg.de");

// Create a hash, it will change each hour.
function getHash(data) {
	var date = new Date();
	var current_hour = date.getHours();
	var data = data + current_hour.toString();
  // Repeat to create some load.
	for (i = 0; i < 1000; i++) {
	  data = data + crypto.createHash("sha512").update(data, "binary").digest("hex")
	}
  return crypto.createHash("sha512").update(data + current_hour.toString(), "binary").digest("hex");
}

var app = express();
app.use(bodyParser.json());

app.get('/', function (req, res) {
	console.log("Request from: " + req.ip)
	res.send("Use http://<hostname>/hash/<id>");
});

app.get('/hash/:id', function (req, res) {
  console.log("Request hash from: " + req.ip)
  var hash=getHash(req.params.id);
  var qr_png = qr.imageSync(hash,{ type: 'png'});
  // Set cache control, otherwise nginx does not cache.
  res.set('Cache-Control', 'public, max-age=31557600, s-maxage=31557600');
  res.write('<html><head></head><body>');
  res.write('<p>' +  hash + '</p>');
  res.write('<img src="data:image/png;base64,' + qr_png.toString('base64') + '" alt="QR Code">');
  res.end('</body></html>');  	
});

/* Using redis cache
app.get('/hash/:id', function (req, res) {
  console.log("Request hash from: " + req.ip)
	res.write('<html><head></head><body>');
  var hash;
  var qr_png;
  cluster.get(req.params.id, (err, result) => {
  // If that key exist in Redis store
		if (result) {
      console.log('from cache');
			var resultJSON = JSON.parse(result);
			hash = resultJSON.hash
      qr_png = resultJSON.qr_png
      res.write('<p>' +  hash + '</p>');
      res.write('<img src="data:image/png;base64,' + qr_png + '" alt="QR Code">');	
      res.end('</body></html>');
    } else { // Key does not exist in Redis store
      console.log('generate');
      hash=getHash(req.params.id);
      qr_png =qr.imageSync(hash,{ type: 'png'}).toString('base64');
      cluster.setex(req.params.id, 1000,JSON.stringify({ 'hash' : hash, 'qr_png' : qr_png }));
      res.write('<p>' +  hash + '</p>');
      res.write('<img src="data:image/png;base64,' + qr_png + '" alt="QR Code">');	
      res.end('</body></html>');	
    }
  });
});
*/

app.listen(3000, "0.0.0.0", function () {
  console.log('Example app listening on port 3000!');
});

