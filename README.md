# Install on Ubuntu 22.04 LTS image

Use snap to install nodejs, the version in the repository is quite old.
```
sudo apt update
sudo snap install node --classic
npm install --save express 
npm install --save body-parser
npm install --save qr-image
npm install --save crypto
npm install --save ioredis
```

## Configure Security Group for the Node App

Add TCP port number 3000 to inbound rules of the related security group.

## Start the app
```
node app.js
```

For a better performance use
```
node app.js > /dev/null
```
but you won't see error messages.

# Usage

The provides a QRCode and a related hash for an id, to get it open the website

http://*hostname*/hash/*id*

Replace *hostname* with the public IP or hostname of your webserver and replace
*id* with any positive integer.

# Testing

You can test with your browser or use Jmeter (version 5.5).

For JMeter install JMeter and the plugin 'Throughput Shaping Timer', adapt the test plan given in `testing/test.jmx`. Run JMeter in GUI mode and open the test plan file `testing/test.jmx`. In the ThreadGroup select the HTTP Request module and change the url and port according to your setup. The number of requests per second are configure in the plugin 'Throughput Shaping Timer'. The test plan is prepared for a AWS micro instance to see response times in underload and overload.

Afterwards run the test in CLI mode (Linux, here version 5.5 is installed in the user directory):
```
/home/ubuntu/apache-jmeter-5.5/bin/jmeter -n -Jjmeter.reportgenerator.overall_granularity=1000 -t ~/git/gethash/testing/test.jmx -l log.jlt -e -o results
```

or (Windows, here version 5.5 is installed in the user directory):

```
C:\Users\Me\Downloads\apache-jmeter-5.5\apache-jmeter-5.5\bin\jmeter.bat -n -Jjmeter.reportgenerator.overall_granularity=1000 -t C:\Users\Me\Downloads\test.jmx -l log.jlt -e -o results
```

If your path contains spaces, you have to escape the path with " " .

View the results with ayn webbrowser, e.g.:
```
firefox results/index.html
```

# Install and configure NGINX on Ubuntu 22.04 LTS image
```
sudo apt update
sudo apt install nginx
sudo unlink /etc/nginx/sites-enabled/default
```

Enabling caching and create reverse proxy in `/etc/nginx/sites-available/cache`

```
proxy_cache_path /tmp/nginx levels=1:2 keys_zone=my_zone:10m inactive=60m;
server {
  listen 80;
  proxy_cache my_zone;
  location / {
    include proxy_params;
    proxy_pass http://172.31.95.243:3000;
  }
}
```
Update the IP address with the IP address of node.js instance.

## Enable site and restart

```
sudo ln -s /etc/nginx/sites-available/cache /etc/nginx/sites-enabled/cache
sudo systemctl restart nginx
```

## Configure Security Group for NGINX

Add TCP port number 80 to inbound rules for HTTP traffic.

# Install Redis Cache
- Follow instructions document at https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-22-04
- Uncomment code already prepared in `app.js` for redis caching , comment legacy function without redis. 

# Testing

You can test with your browser or use Jmeter (version 5.5).

For JMeter install JMeter and the plugin 'Throughput Shaping Timer', adapt the test plan given in `testing/test.jmx`. Run JMeter in GUI mode and open the test plan file `testing/test.jmx`. In the ThreadGroup select the HTTP Request module and change the url and port according to your setup. The number of requests per second are configure in the plugin 'Throughput Shaping Timer'. The test plan is prepared for a AWS micro instance to see response times in underload and overload.

Afterwards run the test in CLI mode:
```
/home/ubuntu/apache-jmeter-5.5/bin/jmeter -n -Jjmeter.reportgenerator.overall_granularity=1000 -t ~/git/gethash/testing/test.jmx -l log.jlt -e -o results
```

View the results with:
```
firefox results/index.html
```




